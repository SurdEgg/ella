# frozen_string_literal: true

# This is the assets pipeline for Javascript. See the documentation for more info. Kill time.
module Ella
  class Pipeline
    def js
      # Welcome to your Javascript filter! Everything here will be outputted into
      # a single file, whose path is available by running the method "js_path",
      # which can be called from a view.

      # The method 'asset_data(*file_names)' will return the data for all files
      # that are in the relevant assets directory. For JS that is "assets/js/"

      # A quick and dirty example of how use might implement a simple minifier:
      # raw_data = asset_data('main.js', 'other_file.js')
      # raw_data.gsub("\n", '')
    end
  end
end
