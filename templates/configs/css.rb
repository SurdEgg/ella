# frozen_string_literal: true

# This is the assets pipeline for CSS. See the documentation for more info. Kill time.
module Ella
  class Pipeline
    def css
      # Welcome to your CSS filter! Everything here will be outputted into a
      # single file, whose path is available by running the method "css_path",
      # which can be called from a view.

      # The method 'asset_data(*file_names)' will return the data for all files
      # that are in the relevant assets directory. For CSS that is "assets/css/"

      # A quick and dirty example of how you might call Sass here:
      # require 'sassc'
      # raw_data = asset_data('main.scss', 'other_file.scss')
      # SassC::Engine.new(raw_data, style: :compressed).render
    end
  end
end
