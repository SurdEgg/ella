# frozen_string_literal: true

# Puma configuration file.
bind 'tcp://localhost:7654'
pidfile File.join(__dir__, '../temp/puma.pid')
