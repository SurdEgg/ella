# frozen_string_literal: true

class RootController < Ella::Controller
  get '/' do
    erb :'root/index'
  end
end
