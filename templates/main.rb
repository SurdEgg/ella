# frozen_string_literal: true

# You can put any code you like in this file and it will load when you run the
# server.
# The Ella server depends on this file existing in the root directory, so please
# do not remove or delete it.
class <%= name.pascal_case %> < Ella::Controller
end
