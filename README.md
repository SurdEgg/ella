# Ella

Ella is a backend web framework using [Ruby](https://www.ruby-lang.org/en/), [Sinatra](http://sinatrarb.com/), [Puma](https://puma.io/), and [Rack](https://github.com/rack/rack). The basic goal is to have some of the basic conveniences of RoR, but cleaner, smaller, more flexible, and with minimal depencies.

Ella is still in early beta, so there are still a few things to work out!

## Table of Contents

* [Ella](#markdown-header-ella)
	* [Table of Contents](#markdown-header-table-of-contents)
	* [Hello World - Simple](#markdown-header-hello-world)
	* [Hello World - Build a Blog](#markdown-build-a-blog)
	* [Docs](#markdown-header-docs)

## Hello World

Doing this will give you a basic web app on port 7654:

```shell
gem install ella
ella new foo
cd foo
ella s
```

## Build A Blog

Just a simple blog to get an idea of how to use Ella:

```shell
ella new test-blog
cd test-blog
ella mvc user index show new destroy
ella mvc post index show new destroy
```
This will create the appropriate files and routes for models, views, and controllers. The actual implementation is up to you, the developer. While this is not as convenient as RoR, it is easier to learn and much more flexible. The primary inspiration for Ella was that very few real-world projects fit the exact RoR model, which requires a lot of ugly hacking to get around.

To pass things from a controller to a view, use instance variables:

```ruby
# models/foo.rb
class Foo
  def some_method
    'Hello World!'
  end
end
```

```ruby
# controllers/foo.rb
get '/foo/new' do
  @this_foo = Foo.new
end
```

```ruby
# views/foo/new.erb
<p><%= @this_foo.some_method %></p>
```

Routing is handled by the controllers, and is taken directly from Sinatra. See the [Sinatra documentation](http://sinatrarb.com/intro.html) for more info.

Ella does not feature an ORM, but an plugin is in the works. In the meantime, try [Sequel](https://github.com/jeremyevans/sequel).

## Docs

To get help:

```shell
ella h
```

To create a new project:

```shell
ella new (project name)
```

To run a server:

```shell
ella s (optional: development/production) (optional: port number)
```
The port can also be set in config/puma.rb

To create a new controller:

```shell
ella c (controller name) (optional: route names)
```

To create a new model:

```shell
ella m (model name) (optional: route names)
```

To create a new view subdirectory:

```shell
ella v (view name) (optional: route names)
```

These can be combined, although a controller is necessary for routing. The following example will just create the controller and the view:

```shell
ella cv foo
```

The css/js pipelines are configured in the "config/" directory, and the assets themselves can be found in the "assets/" directory. The assets config files have more detailed explanations in the comments.

Ella uses RSpec for tests. To run your test suite:

```shell
ella t
```
