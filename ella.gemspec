require_relative 'lib/ella/version'

Gem::Specification.new do |spec|
  spec.name          = "ella"
  spec.version       = Ella::VERSION
  spec.authors       = ["Kyle Church"]
  spec.email         = ["kyle@kylechur.ch"]
  spec.summary       = %q{A convenience-focused mesoframework for web development.}
  spec.description   = %q{Ella is a Sinatra-based web framework which seeks to find a balance between the simplicity of a microframework and the conveniences of a large, opinionated framework.}
  spec.homepage      = "https://bitbucket.org/SurdEgg/ella/"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://bitbucket.org/SurdEgg/ella/"
  spec.metadata["changelog_uri"] = "https://bitbucket.org/SurdEgg/ella/src/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
