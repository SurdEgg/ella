# frozen_string_literal: true

# Tests the generator abstract class.
RSpec.describe '"ella new"' do
  before(:all) do
    `ella new project_to_be_tested`
    Dir.chdir('project_to_be_tested')
  end

  it 'creates a new subdirectory' do
    expect(Dir.entries(File.expand_path("..", Dir.pwd))).to include('project_to_be_tested')
  end

  it 'creates a controller directory' do
    expect(Dir.entries(Dir.pwd)).to include('controllers')
  end

  it 'creates a root controller' do
    expect(Dir.entries('controllers')).to include('root.rb')
  end

  after(:all) do
    Dir.chdir('..')
    `rm -r project_to_be_tested/`
  end
end
