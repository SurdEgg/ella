# frozen_string_literal: true

require_relative 'pipeline'

module Ella
  # This is not so much a server as a something which starts of the server and
  # various listeners and initializers associated with Ella. From the point of
  # view of the rest of Ella, this will be the server, though.
  # This also restarts the server in development mode, something Sinatra is a
  # bit shifty about on its own, due to Ruby restrictions.
  class Server
    def initialize(mode: 'development', port: nil)
      Ella.find_root
      Log.yell("Starting server in #{mode} mode...")
      @mode = mode
      @port = port

      establish_pipelines
    end

    def run
      Ella.find_root
      @mode_path = File.join(Dir.pwd, 'temp/mode')
      File.open(@mode_path, 'w') { |f| f.puts(@mode) }
      RackfileGenerator.new.run

      Log.info('Staring Puma...')
      @mode == 'production' ? start_production_server : start_development_listener_loop
    end

    def css_path
      @pipelines[:css].path
    end

    def js_path
      @pipelines[:js].path
    end

    private

    def start_production_server
      system("puma #{"-p #{@port}" if @port} -e #{@mode} -C \"configs/puma.rb\" rack.ru", out: $stdout, err: :out)
    end

    # As the name implies, it is necessary to keep the program idling while in
    # development, in order have the listeners contiue to run.
    def keep_listeners_alive
      sleep
    rescue Interrupt
      sleep 1
      File.delete(@mode_path) if File.exist?(@mode_path)
      Log.exit_message('Ella is shutting down. Moimoi.')
    end

    # This restarts the actual puma server, but not Ella's listerners, which
    # remain running. Sinatra's reloader only works for files that exist when
    # the server started. To my knowledge this has to be the case.
    # See Puma documentation on SIGUSR2 for more info.
    def restart
      mode = File.open(File.join(Dir.pwd, 'temp/mode')).read.chomp
      if mode == 'development'
        RackfileGenerator.new.run
        pid = File.open(File.join(Dir.pwd, 'temp/puma.pid')).read.to_i
        Process.kill('SIGUSR2', pid)
      end
    end

    # Modified is handled by Sinatra reloader, if anything because the console
    # output is less spammy.
    def start_model_controller_listeners
      input_dirs = [File.join(Dir.pwd, 'controllers'), File.join(Dir.pwd, 'models')]
      @listener = Listen.to(*input_dirs) do |modified, added, removed|
        restart if !added.empty? || !removed.empty?
      rescue => e
        # You never know with listen.
        puts e
      end
      @listener.start
    end

    def start_development_listener_loop
      start_development_server
      start_model_controller_listeners
      keep_listeners_alive
    end

    # Do not refactor unless you are 100% sure you are comfortable with Ruby forking.
    def start_development_server
      unless @pid = fork
        exec("puma #{"-p #{@port}" if @port} -e #{@mode} -C \"configs/puma.rb\" rack.ru", out: $stdout, err: :out)
      end
    end

    def establish_pipelines
      @pipelines = { css: Pipeline.new('css'), js: Pipeline.new('js') }
      # Listener runs pipeline in development, otherwise, the pipeline runs once
      # before starting the server.
      if @mode == 'development'
        Log.info('Starting pipeline listeners...')
        @pipelines.map { |_, v| v.listen }
      else
        Log.info('Precompiling assets...')
        @pipelines.map { |_, v| v.run }
      end
    end
  end
end
