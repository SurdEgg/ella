# frozen_string_literal: true

require 'tempfile'

# Extra-special Message
# ---------------------
#
# I started making Ella because I found writing my own quick asset pipelines
# was much easier and faster and more bug-free than the "modern" and "correct"
# solutions. YAGNI should be a major cornerstorne of Ella.
#
# Rake may be considered iff this file ever becomes overly-complicated or slow,
# but I want to make sure that an easy pipeline is available by default. The
# user can set up another pipeline if they need something more complicated.
#
# -- kmc

module Ella
  # Custom made super-simple assets pipeline. This KISS philosophy of this
  # pipeline is:
  #
  # "data from files" --> "user defined filter in Ruby" --> "output file"
  #
  # Of course, Ella is modular, so the user should be able to disable it and
  # set up their own assets pipeline of choice.
  class Pipeline
    def initialize(pipeline_type)
      @type = pipeline_type

      Log.info("Initializing #{@type.upcase} pipeline...")
      set_io_directories
      initialize_tempfile
      load File.join(Dir.pwd, "configs/#{@type}.rb")
    end

    # Because this is user-defined code, any exception is possible. Having
    # to restart the development server every time there is some error in
    # the filter is *NOT DESIRABLE*.
    def listen
      run # Public files are not persistent, so this must be run on startup.
      @listener = Listen.to(@input_dir) do |modified, added, removed|
        report_listen_results(modified, added, removed)
        run
      rescue => e
        report_listen_error(e)
      end
      @listener.start
    end

    def run
      @tempfile.close
      @tempfile.unlink
      @tempfile = Tempfile.new(['', ".#{@type}"], @output_dir)
      @tempfile.write(filter)
      @tempfile.rewind
    end

    private

    def set_io_directories
      @input_dir = File.join(Dir.pwd, "assets/#{@type}")
      @output_dir = File.join(Dir.pwd, "public/#{@type}")
      Dir.mkdir(@input_dir) unless Dir.exist?(@input_dir)
      Dir.mkdir(@output_dir) unless Dir.exist?(@output_dir)
    end

    def initialize_tempfile
      @tempfile = Tempfile.new(['', ".#{@type}"], @output_dir)
    end

    def report_listen_results(modified, added, removed)
      Log.info("#{@type.capitalize} Pipeline detected change:")
      Log.info("Modified: #{modified}") if modified
      Log.info("Modified: #{added}") if added
      Log.info("Modified: #{removed}") if removed
      Log.info("#{@type.capitalize} Pipeline recompiling")
    end

    def report_listen_error(e)
      Log.error('Error in listener:')
      puts e.backtrace
      puts e.inspect
    end

    def filter
      # TODO: Something more dynamic should go here. People should be able to
      # create their own pipelines.
      if @type == 'css'
        css
      elsif @type == 'js'
        js
      end
    end

    def asset_data(*files)
      files.inject('') { |str, fname| str += File.read(File.join(@input_dir, fname)) }
    end
  end
end
