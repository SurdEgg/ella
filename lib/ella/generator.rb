# frozen_string_literal: true

module Ella
  # This is an abstract class for the numerous generators that make up the bulk
  # of this project. It cannot be run on its own.
  # TODO: The first parameter should be part of the hash, for clarity.
  class Generator
    def initialize(directory: nil, files: [], templates: [], template_vars: {})
      @directory = NameFormatter.new(directory) if directory
      @files = files
      @templates = templates
      @template_vars = template_vars
    end

    def run
      raise NotImplementedError, 'Subclasses must define \'run\'.'
    end

    private

    def generator_type
      self.class.name.gsub(/^.*::(.*)Generator$/, '\1').downcase + 's'
    end

    def make_directory(special_name = nil)
      target = special_name || generator_type
      return if Dir.exist?(target)

      Log.create(target + '/')
      Dir.mkdir(target)
    end

    def make_test_directory
      make_directory('tests')
      make_directory("tests/#{generator_type}")
    end

    def target_name
      @directory ? @directory.snake_case : @directory
    end
  end
end
