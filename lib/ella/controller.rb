# frozen_string_literal: true

require 'sinatra/base'
require_relative 'reloader'

module Ella
  # Sinatra was not specifically designed to have different modular controller
  # files. Some of the magic needs to go here, and maintaining a specific
  # loading order is important: seemingly insignificant changes can break the
  # little hacks that allow Sinatra to be used as a macro-framework.
  class Controller < Sinatra::Base
    set :views, Dir.pwd + '/views' # TODO This can be more dynamic.
    set :public_folder, Dir.pwd + '/public'

    configure :development do
      register Sinatra::Reloader
      also_reload File.join(Dir.pwd, 'models/*')
      also_reload File.join(Dir.pwd, 'controllers/*')
    end

    # TODO: Some fancy-pants metaprogramming to allow for all user-defined
    # pipelines.
    def js_path
      filename = Dir.glob("public/js/*").max_by {|f| File.mtime(f)}
      filename.sub('public', '')
    end

    def css_path
      filename = Dir.glob("public/css/*").max_by {|f| File.mtime(f)}
      filename.sub('public', '')
    end
  end
end
