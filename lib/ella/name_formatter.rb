# frozen_string_literal: true

module Ella
  # Ruby and rubyist convention demand that the project name be formatted in
  # different ways depending on context. This class helps prevent repetition
  # of name formatting.
  # Thousands of Edabit exercises have prepared me for this, my finest hour.
  # I am currently assuming that no one will be initializing in or using
  # camelCase, as that format seems to have very little use in the Ruby
  # community.
  class NameFormatter
    attr_reader :snake_case

    def initialize(name)
      Ella.abort('Project name must be a valid string.') if name.nil? || name.empty?
      # If the project name is given in Pascal Case, save as snake case.
      @snake_case = name =~ /^[A-Z]/ ? name.gsub(/([A-Z])/, '_\1')[1..-1].downcase : name
    end

    def file_name
      "#{@snake_case}.rb"
    end

    def pascal_case
      @snake_case.split('_').map(&:capitalize).join('')
    end

    def human
      @snake_case.split('_').map(&:capitalize).join(' ')
    end
  end
end
