# frozen_string_literal: true

require 'logger'
require 'colorize'

module Ella
  # A convenient minor reworking of Ruby's logger.
  class Log
    # The first three methods do not have timestamps and may be colour coded.
    # They should not be anything less trivial than a heads up for the user.
    def self.error(message)
      say('ERROR: '.red + message.bold)
    end

    def self.newline
      say("\n")
    end

    def self.info(message)
      say("  ELLA: ".bold.blue + message.split(' ').map(&:capitalize).join(' ').bold)
    end

    def self.yell(message)
      say(message.bold.blue)
    end

    def self.exit_message(message)
      say(message.bold.yellow)
    end

    # Misc. It would be best to phase this out in the long run.
    def self.say(message)
      puts message
    end

    def self.unknown(message)
      log_error.unknown(message)
    end

    def self.fatal(message)
      say('FATAL: '.red + message.bold)
    end

    def self.warn(message)
      log_info.warn(message)
    end

    def self.debug(message)
      log_info.info(message)
    end

    def self.create(message)
      output_str = ' ' * 8 +  'CREATE  '.green.bold + message
      say(output_str)
    end

    def self.destroy(message)
      output_str = ' ' * 8 + 'DESTROY  '.red.bold + message
      say(output_str)
    end

    def self.log_info
      Logger.new(STDOUT)
    end

    def self.log_error
      Logger.new(STDERR)
    end
  end
end
