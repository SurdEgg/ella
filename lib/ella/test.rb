module Ella
  # Runs tests for user project. Currently default to RSpec.
  class Test
    def self.run
      Ella.find_root
      exec('rspec -P "tests/*.rb, tests/*/*.rb"')
    end
  end
end
