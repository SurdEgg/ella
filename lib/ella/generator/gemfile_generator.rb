# frozen_string_literal: true

module Ella
  # Generates a Gemfile with the necessary dependencies for an ella project, and runs Bundle to
  # install those dependencies.
  class GemfileGenerator < Generator
    def run
      Ella.find_root
      Ella::Template.new('Gemfile').write
      Log.newline
      Log.info('Running Bundle')
      puts `bundle update`
    end
  end
end
