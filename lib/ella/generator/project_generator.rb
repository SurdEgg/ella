# frozen_string_literal: true

module Ella
  # This beast of a subclass generates a new Ella project from scratch.
  # --------
  # Ella requires a main project file for almost everything. Consider this,
  # anyone who might want to change the order of this method.
  class ProjectGenerator < Generator
    def run
      Log.yell("Generating New Ella Project: \"#{@directory.human}\"")

      prepare_project_directory
      create_main_project_file
      initialize_git
      generate_pipeline_directories
      initialize_config
      generate_model_directory
      generate_views
      generate_controllers
      initialize_bundle
    end

    private

    def prepare_project_directory
      if Dir.entries('.').include?(@directory.snake_case)
        Log.error('A directory with that name already exists. Aborting.')
        abort
      end
      Log.newline
      Log.info('Creating project core')
      make_directory(@directory.snake_case)
      Dir.chdir(@directory.snake_case)
      make_directory('temp')
    end

    def create_main_project_file
      Ella::Template.new('main.rb', template_vars: { name: @directory }).write
      Log.newline
    end

    def initialize_git
      Log.info('Initializing git')
      `git init`
    end

    def generate_pipeline_directories
      Log.info('Generating pipeline directories')
      make_directory('assets')
      make_directory('assets/css')
      make_directory('assets/js')
      make_directory('public')
      make_directory('public/css')
      make_directory('public/js')
      Log.newline
    end

    def initialize_config
      Log.info('Generating configuration files')
      ConfigGenerator.new.run
      Log.newline
    end

    def generate_model_directory
      make_directory('models')
      Log.newline
    end

    def generate_views
      Log.info('Generating views')
      ViewGenerator.new(templates: ['layout'], template_vars: { name: @directory }).run
      ViewGenerator.new(directory: 'root', templates: ['index']).run
      Log.newline
    end

    def generate_controllers
      Log.info('Generating root controller')
      ControllerGenerator.new(directory: 'root').run
      Log.newline
    end

    def initialize_bundle
      Log.info('Creating Gemfile')
      GemfileGenerator.new.run
      Log.newline
    end
  end
end
