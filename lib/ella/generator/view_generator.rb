# frozen_string_literal: true

module Ella
  # Generates views for Ella projects.
  class ViewGenerator < Generator
    def run
      Ella.find_root
      make_directory
      @path = "views/#{target_name}#{target_name ? '/' : ''}"
      create_subdirectory if target_name && !Dir.exist?(@path)
      create_files
      copy_templates
    end

    private

    def full_filename(filename)
      "#{@path}#{filename}.erb"
    end

    def create_file(filename)
      Ella::Template.new(full_filename(filename), generic_template: :blank).write
    end

    def copy_template(filename)
      Ella::Template.new(full_filename(filename), template_vars: @template_vars).write
    end

    def copy_templates
      @templates.each { |filename| copy_template(filename) } if @templates
    end

    def create_files
      @files.each { |filename| create_file(filename) } if @files
    end

    def create_subdirectory
      Log.create(@path)
      Dir.mkdir(@path)
    end
  end
end
