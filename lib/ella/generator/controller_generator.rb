# frozen_string_literal: true

module Ella
  # Generator for controllers.
  class ControllerGenerator < Generator
    def run
      # In the case of a controller, 'directory' and 'files' corresponds to the
      # views being controlled. Every controller file represents a directory,
      # and every controller route represents a file.
      Ella.find_root
      make_directory
      @template_vars[:name] = @directory
      @template_vars[:files] = @files
      generate_controllers
      generate_tests
    end

    private

    # The root controller is a special case and does not use the generic template.
    def generate_controllers
      path = "controllers/#{@directory.snake_case}.rb"
      template = (@directory.snake_case == 'root' ? nil : 'controller')
      Ella::Template.new(path, generic_template: template, template_vars: @template_vars).write
    end

    def generate_tests
      make_test_directory
      path = "tests/controllers/#{@directory.snake_case}_test.rb"
      @template_vars[:controller] = true
      Ella::Template.new(path, generic_template: 'test', template_vars: @template_vars).write
    end
  end
end
