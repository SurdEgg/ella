# frozen_string_literal: true

module Ella
  # This reverses the results of the m/v/c and test generators. Right now it
  # destroys everything of a certain name if found. While that seems fairly
  # blunt, there does not currenlty seem to be a reason to make it more
  # specific.
  # Although this is the opposite of a generator, the initialization requires
  # the exact same things, and it makes sense to have the same interface, so it
  # inherits from Generator.
  class Destroyer < Generator
    def run
      @something_destroyed = false

      attempt_to_destroy('models')
      attempt_to_destroy('views')
      attempt_to_destroy('controllers')
      # TODO: destroy tests, once rspec is set up

      Log.info('Nothing to destroy.') unless @something_destroyed
    end

    private

    def destroy_dir(path)
      Log.destroy(path)
      Dir.delete(path)
      @something_destroyed = true
    end

    def destroy_file(path)
      Log.destroy(path)
      File.delete(path)
      @something_destroyed = true
    end

    def attempt_to_destroy(type)
      dir_path = File.join(Dir.pwd, "#{type}/#{@directory.snake_case}")
      file_path = File.join(Dir.pwd, "#{type}/#{@directory.file_name}")
      destroy_dir(dir_path) if Dir.exist?(dir_path)
      destroy_file(file_path) if File.exist?(file_path)
    end
  end
end
