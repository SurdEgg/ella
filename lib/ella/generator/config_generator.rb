# frozen_string_literal: true

module Ella
  # This class generates Ella project config files, which are needed to nicely
  # configure Puma and Sinatra.
  class ConfigGenerator < Generator
    def run
      Ella.find_root
      make_directory('log') # necessary for Puma config
      make_directory # make config directory
      Ella::Template.new('configs/puma.rb').write
      Ella::Template.new('configs/css.rb').write
      Ella::Template.new('configs/js.rb').write
    end
  end
end
