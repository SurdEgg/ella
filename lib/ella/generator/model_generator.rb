# frozen_string_literal: true

module Ella
  # Generator for models.
  class ModelGenerator < Generator
    def run
      Ella.find_root
      make_directory
      @template_vars[:name] = @directory
      generate_models
      generate_tests
    end

    private

    def generate_models
      path = "models/#{@directory.snake_case}.rb"
      Ella::Template.new(path, generic_template: 'model', template_vars: @template_vars).write
    end

    def generate_tests
      make_test_directory
      path = "tests/models/#{@directory.snake_case}_test.rb"
      Ella::Template.new(path, generic_template: 'test', template_vars: @template_vars).write
    end
  end
end
