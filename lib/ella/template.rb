# frozen_string_literal: true

require 'erb'

module Ella
  # Class to handle naming, copying, and processing the various templates that
  # the Ella generators use.
  class Template
    TEMPLATE_DIRECTORY = File.expand_path('../../templates', __dir__)

    # If generic_template is nil, this class will look for a specific template based on the filename.
    def initialize(filename, generic_template: nil, template_vars: nil)
      @filename = filename
      @destination = File.join(Dir.pwd, @filename)
      @generic_template = generic_template
      @template_vars = template_vars
    end

    def write
      Log.create(@filename)
      if file_exists?
        Log.error('File already exists. Skipping.')
      else
        File.open(@destination, 'w') { |f| f.write(data) }
      end
    end

    private

    def file_exists?
      File.exist?(@destination)
    end

    def render_erb(template)
      @template_vars ? ERB.new(template, nil, '-').result_with_hash(@template_vars) : template
    end

    def template_path
      File.join(TEMPLATE_DIRECTORY, @generic_template || @filename)
    end

    def data
      @generic_template == :blank ? '' : render_erb(File.open(template_path, 'r').read)
    end
  end
end
