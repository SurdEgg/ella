# frozen_string_literal: true

module Ella
  # The code for handling command-line arguments was getting complicated and
  # ugly, so now it is a class.
  # This is the least-integral part of Ella, and everything here should be
  # replaceable without having to change the non-static classes.
  class CLI
    def initialize(args)
      @args = args
      @errors = []

      try_no_args
      try_help
      try_new_project
      try_mvc
      try_destroy
      try_server
      try_test

      exit_with_help('Ella does not understand your command.')
    end

    private

    def try_no_args
      return unless @args[0].nil?

      exit_with_help('No arguments given.')
    end

    def try_help
      return unless ['h', 'help', '?'].include?(@args[0])

      exit_with_help(nil)
    end

    def try_new_project
      return unless ['n', 'new'].include?(@args[0])

      exit_with_help('You need to give your project a name.') if @args[1].nil?
      ProjectGenerator.new(directory: @args[1]).run
      exit
    end

    def try_mvc
      return unless @args[0] =~ /^[mvc]*$/

      ModelGenerator.new(directory: @args[1]).run if @args[0] =~ /m/
      ViewGenerator.new(directory: @args[1], files: @args[2..-1]).run if @args[0] =~ /v/
      ControllerGenerator.new(directory: @args[1], files: @args[2..-1]).run if @args[0] =~ /c/
      exit
    end

    def try_destroy
      return unless ['d', 'destroy'].include?(@args[0])

      Destroyer.new(directory: @args[1]).run
      exit
    end

    def try_server
      return unless ['s', 'server', 'r', 'run'].include?(@args[0])

      Server.new(mode: @args[1] || 'development', port: @args[2]).run
      exit
    end

    def try_test
      return unless ['t', 'test'].include?(@args[0])

      Test.run
      exit
    end

    def exit_with_help(message)
      Log.error(message) if message
      print_help
      exit
    end

    def print_help
      puts <<~HELP
      -------------------
      |   Ella - Help   |
      -------------------

      To create a new project:
          > ella new (project name)

      To run a server:
          > ella s

      To create a new controller:
          > ella c (controller name)

      To create a new model:
          > ella m (model name)

      To create a new view subdirectory:
          > ella v (view name)

      To create a view in a subdirectory:
          > ella v foo bar # (saves to views/foo/bar.erb)

      The model, view, and controller commands can be combined:
          > ella mvc blog_post

      To run tests:
          > ella t
      HELP
    end
  end
end
