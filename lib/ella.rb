# frozen_string_literal: true

require 'listen'

require_relative 'ella/version'
require_relative 'ella/log'
require_relative 'ella/name_formatter'

require_relative 'ella/generator'
require_relative 'ella/generator/project_generator'
require_relative 'ella/generator/model_generator'
require_relative 'ella/generator/view_generator'
require_relative 'ella/generator/controller_generator'
require_relative 'ella/generator/gemfile_generator'
require_relative 'ella/generator/config_generator'
require_relative 'ella/generator/rackfile_generator'
require_relative 'ella/generator/destroyer'

require_relative 'ella/template'
require_relative 'ella/controller'
require_relative 'ella/test'
require_relative 'ella/server'
require_relative 'ella/cli'

# The main module for the Ella framework-framework.
module Ella
  # This custom function exists because, ideally, the program will always abort
  # with a logged message.
  def self.abort(message)
    Log.error(message)
    Kernel.abort
  end

  # Most of what Ella does requires a valid Ella project, however, the user
  # could potentially be in a subdirectory.
  def self.find_root
    until Dir.entries(Dir.pwd).include?('main.rb')
      if Dir.pwd == '/'
        puts 'Error! This is not a valid Ella project directory!'
        exit!
      else
        Dir.chdir('..')
      end
    end
  end
end
